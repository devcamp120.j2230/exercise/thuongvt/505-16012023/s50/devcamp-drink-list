
const express = require("express");
const { drinkClassList, drinkObjectList } = require("./data"); 
const app = express();
// khai báo chạy trên cổng 8000
const prot = 8000;
// khai báo để app đọc được body json
app.use(express.json());

app.get("/drinks-class",(req,res)=>{
    res.json({
        drinkClassList
    })
})

app.get("/drinks-oject",(req,res)=>{
    res.json({
        drinkObjectList
    })
})

app.get("/drinks-class/:drinkId",(req,res)=>{
    let drinkId = req.params.drinkId
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
     drinkId = parseInt(drinkId); 
        res.json ({
            drink: drinkClassList.filter((item)=> item.checkDrinkById(drinkId))
        })
})

app.get("/drinks-oject/:drinkId",(req,res)=>{
    let drinkId = req.params.drinkId
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
     drinkId = parseInt(drinkId); 
        res.json ({
            drink: drinkObjectList.filter((item)=> item.checkDrinkById(drinkId))
        })
})

app.get("/drinks-oject",(req,res)=>{
    let code = req.query.code
    if(code) {
        // Viết hoa code nhập vào
        code = code.toUpperCase();

        res.status(200).json({
            drinks: drinkObjectList.filter((item) => item.code.includes(code))
        })
    } else {
        res.status(200).json({
            drinks: drinkObjectList
        })
    }

})

app.get("/drinks-class",(req,res)=>{
    let code = req.query.code
    if(code) {
        // Viết hoa code nhập vào
        code = code.toUpperCase();

        res.status(200).json({
            drinks: drinkObjectList.filter((item) => item.code.includes(code))
        })
    } else {
        res.status(200).json({
            drinks: drinkObjectList
        })
    }

})

app.listen(prot, ()=>{
    console.log("app listen on prot", prot)
});